package sy.security.server.config.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<DbUser> loadUserByDb = loadUserByDb();
		for (DbUser dbUser : loadUserByDb) {
			if(dbUser.getUsername().equals(username)) {
				return new User(username, dbUser.getPassword(), dbUser.getRoles());
			}
		}
		throw new UsernameNotFoundException("用户不存在");
	}
	
	
	/**
	 * 假设这里是通过数据库查询到的用户信息
	 * @return
	 */
	List<DbUser> loadUserByDb(){
		List<DbUser> dbUsers = new ArrayList<>();
		DbUser user1 = new DbUser();
		user1.setUsername("zhangsan");
		user1.setPassword("$2a$10$KoP6eXcFHSfOWs.b888PAuEodTY5uUPlYvbbr4YvjCyNZrSlzalZ.");
		user1.setRoles(Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
		DbUser user2 = new DbUser();
		user2.setUsername("admin");
		user2.setPassword("$2a$10$Fj0saVDNi/5clr6WBongwul6rsrwOLqPkWAInTOASC73/iuIyOooG");//$2a$10$Fj0saVDNi/5clr6WBongwul6rsrwOLqPkWAInTOASC73/iuIyOooG
		user2.setRoles(AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
		dbUsers.add(user1);
		dbUsers.add(user2);
		return dbUsers;
	}
	
	
	class DbUser {
		
		private String id;
		private String username;
		private String password;
		private String birth;
		private List<GrantedAuthority> roles = new ArrayList<>();
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getBirth() {
			return birth;
		}
		public void setBirth(String birth) {
			this.birth = birth;
		}
		public List<GrantedAuthority> getRoles() {
			return roles;
		}
		public void setRoles(List<GrantedAuthority> roles) {
			this.roles = roles;
		}
	}
}
