/*package sy.security.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;


@Configuration
@EnableResourceServer
public class CustomResourceConfig extends ResourceServerConfigurerAdapter{
	
	 @Override
	  public void configure(HttpSecurity http) throws Exception {
		 http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
			.and().authorizeRequests()
         .antMatchers("/data","/data/**")
         .authenticated(); 
	     // http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
		      //.and()
	        //  .headers().contentTypeOptions().disable().frameOptions().disable()
           //   .and().authorizeRequests().anyRequest().permitAll();
              //动态URL配置
              .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {

					@Override
					public <O extends FilterSecurityInterceptor> O postProcess(O fsi) {
						fsi.setSecurityMetadataSource(filterInvocationSecurityMetadataSource());
						fsi.setAccessDecisionManager(accessDecisionManager());
						return fsi;
					}
              }) ;  //;
	  }
	
	 
	 @Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		
		super.configure(resources);
	}
	
}
*/