package sy.security.server.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@Configuration
@EnableAuthorizationServer
public class MyAuthorizationServerConfigurer extends AuthorizationServerConfigurerAdapter {
	
	@Autowired
	private UserDetailsService  userDetailsService;
	@Autowired
	public AuthenticationManager authenticationManager;
	@Autowired
	private DataSource dataSource;
	
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security
		 //允许所有资源服务器访问公钥端点（/oauth/token_key）
        //只允许验证用户访问令牌解析端点（/oauth/check_token）
    	.tokenKeyAccess("permitAll()")
    	.checkTokenAccess("isAuthenticated()");
	//允许表单认证
	security.allowFormAuthenticationForClients(); //主要是让/oauth/token支持client_id以及client_secret作登录认证

	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		//authenticationManager：认证管理器，当你选择了资源所有者密码（password）授权类型的时候，请设置这个属性注入一个 AuthenticationManager 对象
		//userDetailsService：可定义自己的 UserDetailsService 接口实现
		//authorizationCodeServices：用来设置收取码服务的（即 AuthorizationCodeServices 的实例对象），主要用于 "authorization_code" 授权码类型模式
		//implicitGrantService：这个属性用于设置隐式授权模式，用来管理隐式授权模式的状态
		//tokenGranter：完全自定义授权服务实现（TokenGranter 接口实现），只有当标准的四种授权模式已无法满足需求时
		endpoints
			.authenticationManager(authenticationManager)
			.tokenStore(memoryTokenStore())
			.userDetailsService(userDetailsService);
		endpoints.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);// add get method  获取token时允许的请求方式
		endpoints.reuseRefreshTokens(false);
		super.configure(endpoints);
	//	super.configure(endpoints);
	}
	
	/**
	 * 用于配置一个内存实现或JDBC实现的client details service.其包含的一些重要信息包括:
	 * 		clientId: 必需，客户端ID
	 * 		secret : 对于信任的客户端必需
	 * 		scope : 客户端允许访问资源的范围，如果该字段未定义或为空，则客户端访问范围不受限制。
	 * 		authorizedGrantTypes: 指定客户端支持的grant_type
	 * 		authorities: 客户端所拥有的Spring Security的权限值
	 * 
	 * 
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		/*clients.inMemory().withClient("client_1")
		.authorizedGrantTypes("authorization_code", "client_credentials","implicit","password", "refresh_token").redirectUris("http://localhost:8001/code")
		.scopes("app").secret(passwordEncoder.encode("123456"))//.autoApprove(true)//secret("{noop}123456");
		.and().withClient("client_2")
		.authorizedGrantTypes("authorization_code", "password", "refresh_token")	
		.scopes("app").secret("{noop}123456").autoApprove(true)//secret("{noop}123456");
		.and()
		.withClient("client_user").authorizedGrantTypes("authorization_code","password","refresh_token").scopes("app")
		.secret("{noop}123456").autoApprove(true) //PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("123456")
		.and()
		.withClient("client_resource").authorizedGrantTypes("authorization_code","client_credentials","password","refresh_token").scopes("app")
		.secret(passwordEncoder.encode("123456")).autoApprove(true);*/
		
		clients.withClientDetails(clientDetails());
	}
	
	@Bean
    public ClientDetailsService clientDetails() {
        return new JdbcClientDetailsService(dataSource);
    }
	
	@Bean
	  public TokenStore memoryTokenStore() {
	    return new InMemoryTokenStore();
	    
	  }
}
