package sy.security.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class MySecurityConfig extends  WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/myLogin").defaultSuccessUrl("/index").loginProcessingUrl("/login")
		.and().logout().logoutUrl("/logout").logoutSuccessUrl("/mylogin")
		.and().authorizeRequests()
		.antMatchers("/myLogin","/login").permitAll()
		.antMatchers("/index").hasRole("USER")
		.antMatchers("/admin").hasRole("ADMIN")
		.anyRequest().authenticated().and().csrf().disable();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
	}
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	
	//覆盖 configure (AuthenticationManagerBuilder auth) 方法
	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	
		//auth.
		  // enable in memory based authentication with a user named
		 	// &quot;user&quot; and &quot;admin&quot;									
			/*.inMemoryAuthentication().passwordEncoder(passwordEncoder())
			.withUser("zhangsan").password("$2a$10$KoP6eXcFHSfOWs.b888PAuEodTY5uUPlYvbbr4YvjCyNZrSlzalZ.")
			.roles("USER")
			.and()
			.withUser("admin").password("$2a$10$Fj0saVDNi/5clr6WBongwul6rsrwOLqPkWAInTOASC73/iuIyOooG")
			.roles("USER", "ADMIN");*/
		
		 
	}
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManager();
	}
}
