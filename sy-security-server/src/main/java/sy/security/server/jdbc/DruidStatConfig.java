package sy.security.server.jdbc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;


/**
 * Druid 配置
 * @author ThinkGem
 * @version 2017年11月30日
 */
@Configuration
@ConditionalOnProperty(name="druid.stat.enabled", havingValue="true", matchIfMissing=true)
public class DruidStatConfig {


	/**
	 * 注册DruidFilter拦截
	 */
	@Bean
	public FilterRegistrationBean duridFilter() {
		FilterRegistrationBean bean = new FilterRegistrationBean();
		bean.setFilter(new WebStatFilter());
		bean.addInitParameter("exclusions", "*.css,*.js,*.png,"
				+ "*.jpg,*.gif,*.jpeg,*.bmp,*.ico,*.swf,*.psd,*.htc,*.htm,*.html,"
				+ "*.crx,*.xpi,*.exe,*.ipa,*.apk,*.otf,*.eot,*.svg,*.ttf,*.woff,"
				+ "/druid/*");
		bean.addUrlPatterns("/*");
		return bean;
	}
	
	/**
	 * 注册DruidServlet
	 */
	@Bean
	public ServletRegistrationBean druidServlet() {
		ServletRegistrationBean bean = new ServletRegistrationBean();
		bean.setServlet(new StatViewServlet());
		bean.addUrlMappings("/druid/*");
		Map<String, String> initParameters = new HashMap<String, String>();
	    initParameters.put("loginUsername", "admin");// 用户名
	    initParameters.put("loginPassword", "111111");// 密码
	    initParameters.put("resetEnable", "false");// 禁用HTML页面上的“Reset All”功能
	    initParameters.put("allow", ""); // IP白名单 (没有配置或者为空，则允许所有访问)
	    //initParameters.put("deny", "192.168.20.38");// IP黑名单 (存在共同时，deny优先于allow)
	    bean.setInitParameters(initParameters);
		return bean;
	}
	
}
