package sy.security.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LearnController {
	//xxd
	@RequestMapping("/index")
	public String index() {
		return "/index.html";
	}
	@RequestMapping("/admin")
	public String admin() {
		return "/admin.html";
	}
	@RequestMapping("/myLogin")
	public String myLogin() {
		return "/login.html";
	}
	
}
