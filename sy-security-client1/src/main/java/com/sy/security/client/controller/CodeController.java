package com.sy.security.client.controller;

import java.util.Map;import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;


/**
 * 授权码模式
 * @author LUOWEN
 *
 */
@RestController
public class CodeController {
	
	@Autowired
	public RestTemplate  restTemplate;
	
	
	@RequestMapping("/code")
	public String  getToken(String code,String access_token) {
			if(org.springframework.util.StringUtils.isEmpty(code)) {
				return null;
			}
		 	HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	        MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
	        params.add("client_id","client_1");
	        params.add("client_secret","123456");
	        params.add("grant_type","authorization_code");
	        params.add("scope","app");
	        params.add("code",code);
	        params.add("redirect_uri", "http://localhost:8001/code");
	        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
	        
	       // String postForObject = restTemplate.getForObject("http://localhost:8010/oauth/token", requestEntity, String.class);
	        
	        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8080/oauth/token", requestEntity, String.class);
	        String token = response.getBody();
	 
	        return token;
	}
	
	
	@RequestMapping("myclient")
	public String myClient() {
		return "this is client1";
	}
	
}
