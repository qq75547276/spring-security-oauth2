package com.sy.security.resource.configure;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import com.sy.security.resource.manager.UrlRoleManager;

/**
 * 动态获取url权限配置
 * @author LUOWEN
 *
 */
@Component
public class MyFilterInvocationSecurityMetadataSource implements org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource {
	
	@Autowired
	private UrlRoleManager urlRoleManager;
	
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    private  Map<String,String> urlRoleMap = new HashMap<String,String>();
    
    private Map<String, String> getUrlRoleMap() {
		return urlRoleMap;
	}

	public void setUrlRoleMap(Map<String, String> urlRoleMap) {
		this.urlRoleMap = urlRoleMap;
	}
	
	public Map<String,String> loadMetaSourceMap() {
		return new HashMap<>();
	}
	//此方法是为了判定用户请求的url 是否在权限表中，如果在权限表中，则返回给 decide 方法，用来判定用户是否有此权限。如果不在权限表中则放行。
	@Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        FilterInvocation fi = (FilterInvocation) object;
        String url = fi.getHttpRequest().getRequestURI();
        
        //用户所拥有的url资源 
       // Map<String, String> authorityData = new HashMap<>();
        
        for (Entry<String, String> entry : urlRoleManager.getUrlRoles().entrySet()) {
        	urlRoleMap.put(entry.getKey(), entry.getValue());
		}
        for(Map.Entry<String,String> entry:urlRoleMap.entrySet()){
            if(antPathMatcher.match(entry.getKey(),url)){
                return SecurityConfig.createList(entry.getValue());
            }
        }
        //没有匹配到,默认是要登录才能访问
        return SecurityConfig.createList("ROLE_USER");
        //return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }
}