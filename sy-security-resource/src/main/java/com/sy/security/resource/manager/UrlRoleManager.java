/**
 * 
 */
/**
 * @author LUOWEN
 *
 */
package com.sy.security.resource.manager;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * 假设这个urlRoles 种的数据是从数据库获取的
 * @author LUOWEN
 *
 */
@Component
public class UrlRoleManager {
	
	
	public UrlRoleManager() {
		urlRoles.put("/test/**", "ROLE_ANONYMOUS");//配置两个默认可以访问的
		urlRoles.put("/api/test","ROLE_ADMIN");
	}

	public Map<String,String > urlRoles = new HashMap<>();

	public Map<String, String> getUrlRoles() {
		return urlRoles;
	}

	public void setUrlRoles(Map<String, String> urlRoles) {
		this.urlRoles = urlRoles;
	}
	
}