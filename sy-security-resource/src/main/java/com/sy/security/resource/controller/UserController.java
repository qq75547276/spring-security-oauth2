package com.sy.security.resource.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sy.security.resource.bean.UserInfo;

@RestController
public class UserController {

   // 资源API
    @RequestMapping("/api/userinfo")
    public UserInfo getUserInfo(HttpServletRequest request) {
    	String parameter = request.getParameter("access_token");
    	System.out.println(request.getRequestURL());
        String  user  = (String) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(user);
        return userInfo;
    }
    
 // 资源API
    @RequestMapping("/api/getClientMsg")
    public String getClientMsg(HttpServletRequest request) {
    	String clientId =(String) request.getAttribute("clientId");
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	if(authentication instanceof OAuth2Authentication) {
    		OAuth2Authentication auth2Authentication = (OAuth2Authentication) authentication;
    		clientId = auth2Authentication.getOAuth2Request().getClientId();
    	}
        return clientId;
    }
    
}
