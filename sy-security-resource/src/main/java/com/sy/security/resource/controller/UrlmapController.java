package com.sy.security.resource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sy.security.resource.manager.UrlRoleManager;

@RestController
public class UrlmapController {
	
	@Autowired
	private UrlRoleManager urlRoleManager;
	
	@RequestMapping("/test/admin")
	public String admin() {
		urlRoleManager.getUrlRoles().put("/role/myRole","ROLE_ADMIN");
		return "admin";
	}
	@RequestMapping("/test/user")
	public String user() {
		urlRoleManager.getUrlRoles().put("/role/myRole","ROLE_USER");
		return "user";
	}
	@RequestMapping("/test/other")
	public String other() {
		urlRoleManager.getUrlRoles().put("/role/myRole","ROLE_OTHER");
		return "user";
	}
}
