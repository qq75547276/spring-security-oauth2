package com.sy.security.resource.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
	
	@RequestMapping("/role/myRole")
	public String role() {
		return "myRole";
	}
	
	@RequestMapping("/anonymous")
	public String anonymous() {
		return "anonymous";
	}
	@RequestMapping("/rest/myRest")
	public String rest() {
		return "myRest";
	}
	@RequestMapping("/api/test")
	public String test() {
		return "myTest";
	}
}
