package com.sy.security.resource.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;


@Configuration
@EnableResourceServer
public class CustomResourceConfig extends ResourceServerConfigurerAdapter{
	/* @Primary
	  @Bean
	  public RemoteTokenServices tokenServices() {
	      final RemoteTokenServices tokenService = new RemoteTokenServices();
	      tokenService.setCheckTokenEndpointUrl("http://localhost:8080/oauth/check_token");
	      tokenService.setClientId("client_resource");
	      tokenService.setClientSecret("123456");
	    //  DefaultTokenServices
	      return tokenService;
	  }*/
	//DefaultTokenService中持有TokenStore接口用于保存token。用redis保存可使用redisTokenStore。
	 //RemoteTokenServices 通过访问授权服务器解析令牌-适用 JDBC、内存存储
	
	 @Override
	  public void configure(HttpSecurity http) throws Exception {

	    /*  http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
	              .and()
	              .authorizeRequests()
	              .antMatchers("/anonymous").permitAll()
	              .antMatchers("/api/**").hasRole("ADMIN")
	              .anyRequest()
	              .authenticated()*/
	              /*.and()
	              .requestMatchers()
	              .antMatchers("/role/**")*/;
	             
	      http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
	              .and()
	              .authorizeRequests().anyRequest().authenticated()
	              
	              //动态URL配置
	              .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {

					@Override
					public <O extends FilterSecurityInterceptor> O postProcess(O fsi) {
						fsi.setSecurityMetadataSource(filterInvocationSecurityMetadataSource());
						fsi.setAccessDecisionManager(accessDecisionManager());
					
						return fsi;
					}

					
				}) ;  //;
	  }
	 @Bean
	  public FilterInvocationSecurityMetadataSource filterInvocationSecurityMetadataSource() {
		  return new MyFilterInvocationSecurityMetadataSource();
	  }
	  @Bean
	  public AccessDecisionManager accessDecisionManager() {
		  return new MyAccessDecisionManager();
	  }
	 
	 @Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// resources.
		super.configure(resources);
	}
}
