/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50641
Source Host           : 192.168.0.130:3306
Source Database       : usermanager

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2019-03-20 21:37:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('client_1', null, '$2a$10$Fj0saVDNi/5clr6WBongwul6rsrwOLqPkWAInTOASC73/iuIyOooG', 'app', 'authorization_code,client_credentials,implicit,password,refresh_token', 'http://localhost:8001/code', null, null, null, null, 'true');
INSERT INTO `oauth_client_details` VALUES ('client_resource', null, '$2a$10$Fj0saVDNi/5clr6WBongwul6rsrwOLqPkWAInTOASC73/iuIyOooG', 'app', 'authorization_code,client_credentials,implicit,password,refresh_token', null, null, null, null, null, 'true');
